package gee

import (
	"html/template"
	"net/http"
	"path"
	"strings"
)

type HandlerFunc func(c *Context)

type Gee struct {
	router *router
	*RouterGroup
	groups       []*RouterGroup
	htmlTemplate *template.Template
	funcMap      template.FuncMap
}

func (g *Gee) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	var middlewares []HandlerFunc
	for _, group := range g.groups {
		if strings.HasPrefix(r.URL.Path, group.prefix) {
			middlewares = append(middlewares, group.middlewares...)
		}
	}
	c := newCtx(w, r)
	c.handlers = middlewares
	c.gee = g
	g.router.handle(c)
}

func New() *Gee {
	var gee = &Gee{router: newRouter()}
	gee.RouterGroup = &RouterGroup{gee: gee}
	gee.groups = []*RouterGroup{gee.RouterGroup}
	return gee
}

func (g *Gee) addRoute(method string, pattern string, handler HandlerFunc) {
	g.router.addRoute(method, pattern, handler)
}

func (g *Gee) Get(pattern string, handler HandlerFunc) {
	g.addRoute("GET", pattern, handler)
}

func (g *Gee) Post(pattern string, handler HandlerFunc) {
	g.addRoute("POST", pattern, handler)
}

func (g *Gee) Run(addr string) (err error) {
	return http.ListenAndServe(addr, g)
}

func (g *Gee) SetFuncMap(mapping template.FuncMap) {
	g.funcMap = mapping
}

func (g *Gee) LoadHtmlGlob(pattern string) {
	g.htmlTemplate = template.Must(template.New("").Funcs(g.funcMap).ParseGlob(pattern))
}

type RouterGroup struct {
	prefix      string
	middlewares []HandlerFunc
	parent      *RouterGroup
	gee         *Gee
}

func (group *RouterGroup) Use(middlewares ...HandlerFunc) {
	group.middlewares = append(group.middlewares, middlewares...)
}

func (group *RouterGroup) Group(prefix string) *RouterGroup {
	newGroup := &RouterGroup{
		prefix: group.prefix + prefix,
		parent: group,
		gee:    group.gee,
	}
	group.gee.groups = append(group.gee.groups, newGroup)
	return newGroup
}

func (group *RouterGroup) addRoute(method string, pattern string, handler HandlerFunc) {
	pattern = group.prefix + pattern
	group.gee.router.addRoute(method, pattern, handler)
}

func (group *RouterGroup) GET(pattern string, handler HandlerFunc) {
	group.addRoute("GET", pattern, handler)
}

func (group *RouterGroup) POST(pattern string, handler HandlerFunc) {
	group.addRoute("POST", pattern, handler)
}

func (group *RouterGroup) createStaticHandler(relativePath string, fs http.FileSystem) HandlerFunc {
	absolutePath := path.Join(group.prefix, relativePath)
	fileServer := http.StripPrefix(absolutePath, http.FileServer(fs))
	return func(c *Context) {
		file := c.Param("filepath")
		if _, err := fs.Open(file); err != nil {
			c.Status(http.StatusNotFound)
			return
		}

		fileServer.ServeHTTP(c.Writer, c.Req)
	}
}

func (group *RouterGroup) Static(relativePath, root string) {
	handler := group.createStaticHandler(relativePath, http.Dir(root))
	urlPattern := path.Join(relativePath, "/*filepath")
	group.GET(urlPattern, handler)
}
