package gee

import (
	"net/http"
	"testing"
)

func TestNew(t *testing.T) {
	var g = New()
	g.Get("/hello", func(c *Context) {
		c.String(http.StatusOK, "hello")
	})
	t.Fatal(g.Run(":8080"))
}
